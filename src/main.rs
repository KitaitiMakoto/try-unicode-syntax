use regex_syntax::is_word_character;

fn main() {
    println!("See https://github.com/Plume-org/Plume/issues/757 for the problem");
    println!("Is it word character?");
    let target = "نرم‌افزار";
    println!("Target string: {}", target);
    for char in target.chars() {
	println!("{}: {}", char, is_word_character(char));
    }
    println!("Additional");
    println!("-: {}", is_word_character('-'));
    println!("_: {}", is_word_character('_'));
}
